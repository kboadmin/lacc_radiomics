#LOAD AND PROCESS TRAINING SET (ROME) CLINICAL DATA
daticlinici_rm <- read.csv(file = "RMdata/dat_cervix.csv")
daticlinici_rm[,"TRG"] <- 0
daticlinici_rm[which(daticlinici_rm$risposta.patologica.completa==0),"TRG"] <- 1
daticlinici_rm <- daticlinici_rm[,c("CODICE.SANITARIO","TRG")]
names(daticlinici_rm)[1] <- "patID"
daticlinici_rm$patID <- as.character(daticlinici_rm$patID)
for(st in 1:length(daticlinici_rm$patID)){
  if(nchar(daticlinici_rm$patID[st])==7){
    daticlinici_rm$patID[st] <- paste0("0",daticlinici_rm$patID[st])
  }
}

#LOAD AND PROCESS TRAINING SET (ROME) RADIOMICS FEATURES FROM UNFILTERED IMAGES
load("RMdata/Features.RData")
Features_rm <- Features
Features_rm$patID <- substring(text = Features_rm$patID,first = 4,last = 11)
features_clinici_rm <- merge(daticlinici_rm,Features_rm,by="patID")


#LOAD TRAINING SET (ROME) RADIOMICS FEATURES FROM LoG-FILTERED IMAGES
load("RMdata/Features_log.RData")
for(sigma in names(Features_log)){
  Features_log[[sigma]]$patID <- as.numeric(substring(text = Features_log[[sigma]]$patID,first = 4,last = 11))
  n_feat <- dim(Features_log[[sigma]])[2]
  names(Features_log[[sigma]])[2:n_feat] <- paste0(names(Features_log[[sigma]])[2:233],paste0("_",sigma))
  features_clinici_rm <- merge(features_clinici_rm,Features_log[[sigma]],by="patID")
}

#LOAD TRAINING SET (ROME) FRACTAL FEATURES
load("RMdata/fractals_rm.RData")
ReportGlobale[,"patID"] <- row.names(ReportGlobale)
frattali_features_clinici_rm <- merge(features_clinici_rm,ReportGlobale,by="patID")


#LOAD AND PROCESS TESTING SET (CAMPOBASSO) CLINICAL DATA
daticlinici_cb <- read_excel("CBdata/dati pazienti LACC per studio inviati il 13-2-19.xlsx")
daticlinici_cb[,"TRG"] <- 0
daticlinici_cb[which(daticlinici_cb$`risposta patologica completa`==0),"TRG"] <- 1
daticlinici_cb <- daticlinici_cb[,c("CRT","TRG")]
names(daticlinici_cb)[1] <- "patID"
daticlinici_cb$patID <- substring(text = daticlinici_cb$patID,first =1,last = 4)
daticlinici_cb$patID <- as.character(daticlinici_cb$patID)

#LOAD TESTING SET (CAMPOBASSO) RADIOMICS FEATURES FROM UNFILTERED IMAGES
load("CBdata/features.RData")
Features_cb <- matrice
Features_cb$patID <- as.character(Features_cb$patID)
Features_cb$patID <- substring(text = Features_cb$patID,first = 15,last = 18)
features_clinici_cb <- merge(daticlinici_cb,Features_cb,by="patID")

#LOAD TESTING SET (CAMPOBASSO) RADIOMICS FEATURES FROM LoG-FILTERED IMAGES
load("CBdata/tmp.f.extractor.pluri.par.RData")
for(sigma in names(big.matrice)){
  big.matrice[[sigma]]$patID <- as.numeric(substring(text = big.matrice[[sigma]]$patID,first = 15,last = 18))
  n_feat <- dim(big.matrice[[sigma]])[2]
  names(big.matrice[[sigma]])[2:n_feat] <- paste0(names(big.matrice[[sigma]])[2:233],paste0("_",sigma))
  features_clinici_cb <- merge(features_clinici_cb,big.matrice[[sigma]],by="patID")
}

#LOAD TESTING SET (CAMPOBASSO) FRACTAL FEATURES
load("CBdata/fractals_CB.RData")
ReportGlobale[,"patID"] <- row.names(ReportGlobale)
frattali_features_clinici_cb <- merge(features_clinici_cb,ReportGlobale,by="patID")


### START THE ANALYSIS
library(caret)
library(pROC)
library(readxl)
source("./analytics.R")
outcome_var <- "TRG"
set.seed(12345)

# train data: frattali_features_clinici_rm
dati_clean_train <- analytics.cleanData(data = frattali_features_clinici_rm, outcome = outcome_var,to_drop = c("patID",todrop), na_thres = 0)
dati_subset_train <- analytics.featureSelection(data = dati_clean_train, outcome = outcome_var, 
                                                sign_tresh = 0.07, count_tresh = 1, cor_tresh = 0.6, n_folds = 5)
train_results <- analytics.trainModels(data = dati_subset_train$selectedDati, outcome = outcome_var, n_folds = 5, repeats = 5, upsampling = T)

# test data: frattali_features_clinici_cb
test_results <- analytics.testModels(data = frattali_features_clinici_cb, outcome = outcome_var, train_results = train_results)
report <- analytics.createReport(dati_subset_train$selectedDati, outcome_var, train_results, test_results)


####################
### VIEW RESULTS ###
####################

####################
### TRAIN-CV     ###
####################

# plot Kappa values train-cv
densityplot(train_results$resample_results , metric = "Kappa" ,auto.key = list(columns = 3))
# plot Accuracy values train-cv
densityplot(train_results$resample_results , metric = "Accuracy" ,auto.key = list(columns = 3))
# plot all (higher is better) train-cv
bwplot(train_results$resample_results , metric = c("Kappa","F1"))
#cross-validation rocs
for(model in names(train_results$crossval_roc)){
  plot(train_results$crossval_roc[[model]], main=(paste0(paste0(model," AUC: "), round(as.numeric(train_results$crossval_roc[[model]]$auc),2))))
}


####################
### TEST         ###
####################
#plot the ROC on testing set for the different models
for(model in names(test_results$test_roc)){
  plot(test_results$test_roc[[model]], main=(paste0(paste0(model," AUC: "), round(as.numeric(test_results$test_roc[[model]]$auc),2))))
}

#Create testing set report (a dataframe of performance metrics for the different models)
dataset_cm <- as.data.frame(matrix(ncol = 1+length(test_results$test_confusionmatrix_thr[[1]]$overall)))
names(dataset_cm) <- c(names(test_results$test_confusionmatrix_thr[[1]]$overall),"model")
count <- 2
for(modello in names(test_results$test_confusionmatrix_thr)){
  dataset_cm <- rbind(dataset_cm, test_results$test_confusionmatrix_thr[[modello]]$overall)
  dataset_cm[count,"model"] <- modello
  count <- count + 1
}
dataset_cm <- dataset_cm[-1,]
dataset_cm[order(dataset_cm$Accuracy, decreasing = T),]


##HISTOGRAM OF TRAINING-CV AND TESTING AUC VALUES FOR THE DIFFERENT MODELS
library(reshape2)
new.df<-melt(report$auc_report[order(report$auc_report$AUC_test, decreasing = T),],id.vars="Modello")
names(new.df)=c("Model","AUC_type","AUC_value")
labels_x <- element_text(color = "black", size = 14)
labels_y <- element_text(color = "black", size = 14)
ggplot(new.df, aes(fill=AUC_type, y=AUC_value, x=Model)) + 
  geom_bar(position="dodge", stat="identity",width = .6) +
  coord_cartesian(ylim=c(.5,.85)) + theme(axis.text.x = labels_x, axis.text.y = labels_y,axis.title.x = labels_x,axis.title.y = labels_y)


#VISUALIZE RANDOM FOREST VARIABLE IMPORTANCE
library(ggplot2)
x<-varImp(train_results$models$RF_DEF, scale = TRUE)
rownames(x$importance)
importance <- data.frame(rownames(x$importance), x$importance$Overall)
names(importance)<-c('Covariate', 'Importance')
importance <- transform(importance, Covariate = reorder(Covariate, Importance))
importance <- importance[which(importance$Importance > 50),]
labels_y <- element_text(color = "black", size = 14)
ggplot(data=importance, aes(x=Covariate, y=Importance)) +
  geom_bar(stat = 'identity',colour = "blue", fill = "white") + coord_flip() + theme(axis.text.y = labels_y)
